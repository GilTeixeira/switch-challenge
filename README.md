# README #

A payments service with an architecture based on microservices using Django as the main web application framework and PostgreSQL as the main relational database.


### How do I get set up? ###

    git clone https://bitbucket.org/GilTeixeira/switch-challenge.git
    cd switch-challenge    
    docker-compose up

### How to run tests? ###

    docker-compose run web python manage.py test

### API Description ###
A postman collection is with all routes is available at: `docs/docs/Payments_API.postman_collection.json`

#### List Payments

* **URL** : `/payments/`

* **Method** : `GET`

* **Success Status Code** : `200`

* **Success Response Body ** :

```json
[
    {
        "payment_id": "7a617234-fc09-4d64-aa4d-4dae44bc25d5",
        "amount": 20.0,
        "payment_method": "mbway",
        "created_at": "2021-03-09T15:31:29.837Z",
        "status": "success",
        "settled_at": null,
        "settled_amount": null
    },
    {
        "payment_id": "a53a808f-1039-4122-ac08-3df6f90290c2",
        "amount": 10.0,
        "payment_method": "credit_card",
        "created_at": "2021-03-09T15:31:54.854Z",
        "status": "success",
        "settled_at": null,
        "settled_amount": null
    }
]
```


#### Retrieve a Payment

* **URL** : `/payments/{uuid:payment_id}/`

* **Method** : `GET`

* **Success Status Code** : `200`

* **Success Response Body ** :

```json
{
    "payment_id": "7a617234-fc09-4d64-aa4d-4dae44bc25d5",
    "amount": 20.0,
    "payment_method": "mbway",
    "created_at": "2021-03-09T15:31:29.837Z",
    "status": "success",
    "settled_at": null,
    "settled_amount": null,
    "phone_number": "9111111111"
}
```

#### Search Payments

* **URL** : `/payments?{query}/`

* **Method** : `GET`

* **Example Query ** : `amount__lt=15&created_at__gte=2021-03-09T15:31:54.854Z`

* **Success Status Code** : `200`

* **Success Response Body ** :

```json
[]
```

#### Create a payment

* **URL** : `/payments?{query}/`

* **Method** : `POST`

* **Example Body ** : 
```json
{
    "amount": 10,
    "payment_method": "credit_card",
    "status": "success",
    "number": "4111111111111111",
    "name": "John Doe",
    "expiration_month": "10",
    "expiration_year": "2042",
    "cvv": "123"
}
```

* **Success Status Code** : `201`

* **Success Response Body ** :

```json
{
    "payment_id": "57d621bc-8da8-42b5-a3bd-eff9803bb8ad",
    "amount": 10.0,
    "payment_method": "credit_card",
    "created_at": "2021-03-09T15:45:52.182Z",
    "status": "success",
    "settled_at": null,
    "settled_amount": null,
    "number": "4111111111111111",
    "name": "John Doe",
    "expiration_month": "10",
    "expiration_year": "2042",
    "cvv": "123"
}
```

#### Settle a Payment

* **URL** : `/payments/`

* **Method** : `PATCH`

* **Example Body ** : 
```json
{
    "settled_amount": 203
}
```

* **Success Status Code** : `200`

* **Success Response Body ** :

```json
{
    "payment_id": "462e7519-58f1-4702-a412-2df8fd05f3ad",
    "amount": 10.0,
    "payment_method": "credit_card",
    "created_at": "2021-03-09T16:00:24.418Z",
    "status": "settled",
    "settled_at": "2021-03-09T16:00:31.202",
    "settled_amount": 203.0,
    "number": "4111111111111111",
    "name": "John Doe",
    "expiration_month": "10",
    "expiration_year": "2042",
    "cvv": "123"
}
```

#### Delete a Payment

* **URL** : `/payments/{uuid:payment_id}/`

* **Method** : `DEL`

* **Success Status Code** : `200`

* **Success Response Body ** :

```json
{}
```



### Test Plan ###

#### Basic positive tests (happy paths)
* List Payments
* Create a payment
* Retrieve a Payment
* Search Payments
* Settle a Payment
* Delete a Payment

#### Negative testing – valid input 
* Retrieve a Payment with a non existing id
* Settle a settled payment
* Settle a sucessful payment
* Delete a Payment with a non existing id


#### Negative testing – invalid input
* Create a payment with a negative amount
* Create a payment with a invalid payment method
* Create a payment with a invalid status
* Create a payment with a missing base parameters
* Create a payment with a missing additional parameters
* Retrieve a Payment with a invalid uuid
* Search payments with invalid parameters
* Settle a payment with a negative settled amount

