import json
import uuid
from django.utils import timezone
from django.test import TestCase, Client
from ..models import CreditCard, MBWay

# Create your tests here.


class PaymentTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        pass

    def setUp(self):
        self.client = Client()

        self.base_atts = set(
            [
                'payment_id',
                'amount',
                'payment_method',
                'created_at',
                'status',
                'settled_at',
                'settled_amount'
            ]
        )

        self.cc_atts = set(
            [
                'number',
                'name',
                'expiration_month',
                'expiration_year',
                'cvv'
            ]
        ).union(self.base_atts)

        self.mbw_atts = set(
            [
                'phone_number',
            ]
        ).union(self.base_atts)

    def seed_db(self):
        payments = []
        payments.append(CreditCard.objects.create(
            amount=100, payment_method='credit_card',
            status='success',
            number='4111111111111111', name='John Doe', expiration_month='10',
            expiration_year='2042', cvv='123'
        ))
        payments.append(CreditCard.objects.create(
                        amount=50, payment_method='credit_card',
                        status='error',
                        number='5111111111111111', name='Jane Doe', expiration_month='5',
                        expiration_year='2041', cvv='321'
                        ))
        payments.append(MBWay.objects.create(
                        amount=75, payment_method='mbway',
                        status='error',
                        phone_number='910000000'
                        ))
        payments.append(MBWay.objects.create(
                        amount=25, payment_method='mbway',
                        status='success',
                        phone_number='920000000'
                        ))

        return payments


class PaymentSuccessTestCase(PaymentTestCase):

    def test_list_payments(self):
        response = self.client.get('/payments/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [])

        self.seed_db()

        response = self.client.get('/payments/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 4)

        for payment in response.json():
            self.assertEqual(self.base_atts, payment.keys())

    def test_create_payments(self):
        response = self.client.get('/payments/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [])

        cc_body = json.dumps({
            'amount': 100,
            'payment_method': 'credit_card',
            'status': 'success',
            'number': '4111111111111111',
            'name': 'John Doe',
            'expiration_month': '10',
            'expiration_year': '2042',
            'cvv': '123'
        })

        mbw_body = json.dumps({
            'amount': 10,
            'payment_method': 'mbway',
            'status': 'success',
            'phone_number': '910000000'
        })

        cc_response = self.client.post(
            '/payments/', cc_body, content_type='application/json')
        self.assertEqual(cc_response.status_code, 201)

        mbw_response = self.client.post(
            '/payments/', mbw_body, content_type='application/json')
        self.assertEqual(mbw_response.status_code, 201)

        response = self.client.get('/payments/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 2)

    def test_retrieve_payments(self):
        payments = self.seed_db()

        for payment in payments:
            response = self.client.get(f'/payments/{payment.payment_id}/')
            response_payment = response.json()

            self.assertEqual(response.status_code, 200)
            if payment.payment_method == 'mbway':
                self.assertEqual(self.mbw_atts, response_payment.keys())
            if payment.payment_method == 'credit_card':
                self.assertEqual(self.cc_atts, response_payment.keys())

    def test_search_payments(self):
        payments = self.seed_db()

        response = self.client.get(
            '/payments/', {'payment_id': payments[0].payment_id})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 1)

        response = self.client.get(
            '/payments/', {'amount__gt': 30, 'amount__lt': 70})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 1)

        response = self.client.get(
            '/payments/', {'payment_method': 'mbway'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 2)

        response = self.client.get(
            '/payments/', {'status': 'success'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 2)

        response = self.client.get(
            '/payments/', {'created_at__lt': timezone.now()})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()), 4)

    def test_settle_payments(self):
        payments = self.seed_db()

        for payment in payments:
            if payment.status == 'error':
                response = self.client.patch(f'/payments/{payment.payment_id}/settlement',
                                             {'settled_amount': 30},
                                             content_type='application/json')
                response_payment = response.json()

                self.assertEqual(response_payment['settled_amount'], 30)
                self.assertEqual(response_payment['status'], 'settled')
                self.assertEqual(response.status_code, 200)

    def test_delete_payments(self):
        payments = self.seed_db()

        for payment in payments:
            response = self.client.delete(f'/payments/{payment.payment_id}/')
            self.assertEqual(response.status_code, 204)

        response = self.client.get('/payments/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [])


class PaymentFailureValidInputTestCase(PaymentTestCase):

    def test_retrieve_non_existing_payment(self):
        payment_id = uuid.uuid4()
        response = self.client.get(f'/payments/{payment_id}/')

        expected_error = {'errors': [
            {'payment_id': f'It does not exist an payment with payment_id={payment_id}'}]}

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), expected_error)

    def test_settle_settled_payments(self):
        payments = self.seed_db()

        for payment in payments:
            if payment.status == 'error':
                self.client.patch(f'/payments/{payment.payment_id}/settlement',
                                  {'settled_amount': 30}, content_type='application/json')

                response = self.client.patch(f'/payments/{payment.payment_id}/settlement',
                                             {'settled_amount': 30},
                                             content_type='application/json')

                self.assertEqual(response.status_code, 400)
                self.assertEqual(response.json(), {'errors': [
                                 'Can only settle payments with errors']})

    def test_settle_successful_payments(self):
        payments = self.seed_db()

        for payment in payments:
            if payment.status == 'success':
                self.client.patch(f'/payments/{payment.payment_id}/settlement',
                                  {'settled_amount': 30}, content_type='application/json')

                response = self.client.patch(f'/payments/{payment.payment_id}/settlement',
                                             {'settled_amount': 30},
                                             content_type='application/json')

                self.assertEqual(response.status_code, 400)
                self.assertEqual(response.json(), {'errors': [
                                 'Can only settle payments with errors']})

    def test_delete_non_existing_payment(self):
        payment_id = uuid.uuid4()
        response = self.client.delete(f'/payments/{payment_id}/')

        expected_error = {'errors': [
            {'payment_id': f'It does not exist an payment with payment_id={payment_id}'}]}

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), expected_error)


class PaymentFailureInvalidInputTestCase(PaymentTestCase):
    def test_create_payment_negative_amount(self):
        mbw_body = json.dumps({
            'amount': -10,
            'payment_method': 'mbway',
            'status': 'success',
            'phone_number': '910000000'
        })

        mbw_response = self.client.post(
            '/payments/', mbw_body, content_type='application/json')

        expected_error = {'errors': {'amount': [
            'Ensure this value is greater than or equal to 0.']}}

        self.assertEqual(mbw_response.status_code, 400)
        self.assertEqual(mbw_response.json(), expected_error)

        response = self.client.get('/payments/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [])

    def test_create_payment_invalid_payment_method(self):
        mbw_body = json.dumps({
            'amount': 10,
            'payment_method': 'paypal',
            'status': 'success',
            'phone_number': '910000000'
        })

        mbw_response = self.client.post(
            '/payments/', mbw_body, content_type='application/json')

        expected_error = {'errors': [
            {'payment_method': 'Invalid Payment Method'}]}

        self.assertEqual(mbw_response.status_code, 400)
        self.assertEqual(mbw_response.json(), expected_error)

        response = self.client.get('/payments/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [])

    def test_create_payment_invalid_status(self):
        mbw_body = json.dumps({
            'amount': 10,
            'payment_method': 'mbway',
            'status': 'succex',
            'phone_number': '910000000'
        })

        mbw_response = self.client.post(
            '/payments/', mbw_body, content_type='application/json')

        expected_error = {'errors': {'status': [
            "Value 'succex' is not a valid choice."]}}

        self.assertEqual(mbw_response.status_code, 400)
        self.assertEqual(mbw_response.json(), expected_error)

        response = self.client.get('/payments/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [])

    def test_create_payment_missing_base_parameters(self):
        mbw_body = json.dumps({
            'amount': 10,
            'payment_method': 'mbway',
            'phone_number': '910000000'
        })

        mbw_response = self.client.post(
            '/payments/', mbw_body, content_type='application/json')

        expected_error = {'errors': {'status': ['This field cannot be null.']}}

        self.assertEqual(mbw_response.status_code, 400)
        self.assertEqual(mbw_response.json(), expected_error)

        response = self.client.get('/payments/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [])

    def test_create_payment_missing_additional_parameters(self):
        mbw_body = json.dumps({
            'amount': 10,
            'payment_method': 'mbway',
            'status': 'success',
        })

        mbw_response = self.client.post(
            '/payments/', mbw_body, content_type='application/json')

        expected_error = {'errors': {
            'phone_number': ['This field cannot be null.']}}

        self.assertEqual(mbw_response.status_code, 400)
        self.assertEqual(mbw_response.json(), expected_error)

        cc_body = json.dumps({
            'amount': 100,
            'payment_method': 'credit_card',
            'status': 'success',
            'number': '4111111111111111',
            'name': 'John Doe',
            'expiration_month': '10',
            'cvv': '123'
        })

        cc_response = self.client.post(
            '/payments/', cc_body, content_type='application/json')

        expected_error = {'errors': {
            'expiration_year': ['This field cannot be null.']}}

        self.assertEqual(cc_response.status_code, 400)
        self.assertEqual(cc_response.json(), expected_error)

        response = self.client.get('/payments/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [])

    def test_retrieve_payment_invalid_id(self):
        invalid_id = 2

        response = self.client.get(f'/payments/{invalid_id}/')

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {'errors': [
                         {'payment_id': 'It must be a valid uuid4'}]})

    def test_search_payments_invalid_parameter(self):
        invalid_parameter = 'payment_idd'

        response = self.client.get(
            '/payments/', {f'{invalid_parameter}': 112})

        expected_error = {
            "msg": {
                "Invalid Parameter": f"Cannot resolve keyword '{invalid_parameter}' into field. " +
                'Choices are: amount, created_at, creditcard, mbway, ' +
                'payment_id, payment_method, settled_amount, settled_at, status'
            }
        }

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), expected_error)

    def test_settle_payment_negative_amount(self):
        payments = self.seed_db()

        for payment in payments:
            if payment.status == 'error':
                response = self.client.patch(f'/payments/{payment.payment_id}/settlement',
                                             {'settled_amount': -30},
                                             content_type='application/json')

                expected_error = {'errors': {'settled_amount': [
                    'Ensure this value is greater than or equal to 0.']}}
                self.assertEqual(response.status_code, 400)
                self.assertEqual(response.json(), expected_error)
