import uuid
from django.db import models
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator

# Create your models here.

class Payment(models.Model):
    PAYMENT_METHODS = (
        ('credit_card', 'Credit Card'),
        ('mbway', 'MB WAY'),
    )

    STATUS_VALUES = (
        ('success', 'Success'),
        ('error', 'Error'),
        ('settled', 'Settled'),
    )

    # Base parameters
    payment_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    amount = models.FloatField(validators=[MinValueValidator(0)])
    payment_method = models.CharField(max_length=50, choices=PAYMENT_METHODS)
    created_at = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=50, choices=STATUS_VALUES)
    settled_at = models.DateTimeField(null=True, blank=True)
    settled_amount = models.FloatField(
        null=True, blank=True, validators=[MinValueValidator(0)])

    def clean(self):
        super().clean()
        if self.status == 'settled' and (not self.settled_amount or not self.settled_at):
            raise ValidationError(
                'Fields "settled_amount" and "settled_at" are required for settled payments')

    def serialize(self):
        data = {
            'payment_id': self.payment_id,
            'amount': self.amount,
            'payment_method': self.payment_method,
            'created_at': self.created_at,
            'status': self.status,
            'settled_at': self.settled_at,
            'settled_amount': self.settled_amount
        }
        return data

class CreditCard(Payment):
    number = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    expiration_month = models.CharField(max_length=255)
    expiration_year = models.CharField(max_length=255)
    cvv = models.CharField(max_length=255)

    def serialize(self):
        data = super().serialize()

        data |= {'number': self.number, 'name': self.name,
                    'expiration_month': self.expiration_month,
                    'expiration_year': self.expiration_year, 'cvv': self.cvv}

        return data


class MBWay(Payment):
    phone_number = models.CharField(max_length=255)
    def serialize(self):
        data = super().serialize()

        data |= {'phone_number': self.phone_number}

        return data
