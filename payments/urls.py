from django.urls import path

from .views import PaymentView, PaymentSpecificView, PaymentSettleView

urlpatterns = [
    path('payments/', PaymentView.as_view()),
    path('payments/<str:payment_id>/', PaymentSpecificView.as_view()),
    path('payments/<str:payment_id>/settlement', PaymentSettleView.as_view()),
]
