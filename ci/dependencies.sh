#!/usr/bin/env sh

set -eu

# Add python pip and bash
apk add --no-cache py-pip bash python3-dev
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
# Install docker-compose via pip
pip install --no-cache-dir docker-compose
echo "YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY"

docker-compose -v
